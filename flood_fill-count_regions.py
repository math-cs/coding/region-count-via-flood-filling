"""REGION COUNT VIA FLOOD FILLING, by Ehssan
Generates a random image with boundaries
and counts the number of created regions by flood filling the image."""

from numpy.random import choice

WIDTH = 20
HEIGHT = 5
image = [choice(['#', '-', '-'], WIDTH) for i in range(HEIGHT)]


def flood_fill(i, j, color='o', boundary='#', flag=False):
    # Start at image[i][j] and flood fill its neighborhood with the given color.
    # Don't modify the boundaries.
    if image[i][j] in (color, boundary):
        return flag
    image[i][j] = color
    flag = True
    # if not flag:
    #     flag = True
    if i > 0:
        flood_fill(i-1, j, color, boundary, flag)
    if i < HEIGHT - 1:
        flood_fill(i+1, j, color, boundary, flag)
    if j > 0:
        flood_fill(i, j-1, color, boundary, flag)
    if j < WIDTH - 1:
        flood_fill(i, j+1, color, boundary, flag)
    return flag


def print_image(image):
    for row in image:
        print(''.join(row))
    print()

# Uncomment to test the flood fill function.
# print_image(image)
# flood_fill(0, 0, 'x')
# print_image(image)


def count_regions(image, color='o', boundary='#'):
    count = 0
    for i in range(HEIGHT):
        for j in range(WIDTH):
            flag = flood_fill(i, j, color=color, boundary=boundary)
            if flag:
                count += flag
                # Display the regions filled in at each step.
                print_image(image)
    print(f'The image has {count} region{"" if count==1 else "s"}.')


print_image(image)
count_regions(image)
